const {
    tiket
} = require('../../models/mysql');
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator');

module.exports = {
    getOne: [
        check('id').custom(value => {
            return tiket.findOne({
                _id: value
            }).then(result => {
                if (result.length == 0) {
                    throw new Error('ID tiket tidak ada!')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],
    create: [
        check('nama').isString(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],
    pending: [
        check('id').custom(value => {
            return tiket.findOne({
                _id: value
            }).then(result => {
                if (result.length == 0) {
                    throw new Error('ID tiket tidak ada!')
                }
            })
        }),
        check('nama').isString(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],
    closed: [
        check('id').custom(value => {
            return tiket.findOne({
                _id: value
            }).then(result => {
                if (result.length == 0) {
                    throw new Error('ID tiket tidak ada!')
                }
            })
        }),
        check('nama').isString(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],
    delete: [
        check('id').custom(value => {
            return tiket.findOne({
                _id: value
            }).then(result => {
                if (result.length == 0) {
                    throw new Error('ID tiket tidak ada!')
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ]
}