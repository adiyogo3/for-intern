const express = require('express') // Import express
const router = express.Router() // Make router from app
const TiketController = require('../controllers/tiketController') // Import TransaksiController
const tiketValidator = require('../middlewares/validators/tiketValidator') // Import validator to validate every request from user
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport strategy

// If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/', [passport.authenticate('tiket', {
  session: false
})], TiketController.getAll)

// If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.get('/:id', [passport.authenticate('tiket', {
  session: false
}), tiketValidator.getOne], TiketController.getOne)

// If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.post('/create', [passport.authenticate('tiket', {
  session: false
}), tiketValidator.create], TiketController.create)

// If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.put('/pending/:id', [passport.authenticate('pending', {
  session: false
}), tiketValidator.pending], TiketController.pending)

router.put('/closed/:id', [passport.authenticate('closed', {
    session: false
  }), tiketValidator.closed], TiketController.closed)

// If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class
router.delete('/delete/:id', [passport.authenticate('tiket', {
  session: false
}), tiketValidator.delete], TiketController.delete)

module.exports = router; // Export router
