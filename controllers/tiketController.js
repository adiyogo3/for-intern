const {tiket} = require('../models/mysql/tiket');

class TiketController {
  async getAll(req,res) {
    tiket.find({}).then(result=>{
      res.json({
        status:"Succes get all data",
        data:result
      })
    })
  }
  async getOne(req,res) {
    tiket.findOne({
      _id:req.params.id
    }).then(result=>{
      res.json({
        status:"Succes get data",
        data:result
      })
    })
  }
  async create(req,res) {
    tiket.create({
      nama:req.body.nama
    }).then(result=>{
      res.json({
        status:"Succes create new data",
        data:result
      })
    })
  }
  async pending(req,res) {
    tiket.findOneAndPending({
      _id:req.params.id
    }, {
      nama:req.body.nama
    }).then(()=>{
      return tiket.findOne({
        _id:req.params.id
      })
    }).then(result=>{
      res.json({
        status:"Succes update data",
        data:result
      })
    })
  }
  async closed(req,res) {
    tiket.findOneAndClosed({
      _id:req.params.id
    }, {
      nama:req.body.nama
    }).then(()=>{
      return tiket.findOne({
        _id:req.params.id
      })
    }).then(result=>{
      res.json({
        status:"Succes update data",
        data:result
      })
    })
  }
  async delete(req,res) {
    tiket.delete({
      _id:req.params.id
    }).then(result=>{
      res.json({
        status:"Succes delete data",
        data:null
      })
    })
  }
}

module.exports = new TiketController;
