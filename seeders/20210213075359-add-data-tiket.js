'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Tiket', [{
      nama: "tiket example 1",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      nama: "tiket example 2",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      nama: "tiket example 3",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      nama: "tiket example 4",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      nama: "tiket example 5",
      createdAt: new Date(),
      updatedAt: new Date(),
    }, 
  ])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Tiket', null, {})
  }
};
